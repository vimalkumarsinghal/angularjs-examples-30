;(function () {

  'use strict';

  angular.module('directiveExamples', [
    'serviceExamples'
  ])
    .directive('abcCompany', function () {
    
      return {
        templateUrl: 'app/directiveExamples/abcCompany.html',
        scope: {},
        bindToController: {
          company: '=',
          editing: '&'
        },
        controller: function () {
          var vm = this;
          vm.isEditable = vm.editing();
        },
        controllerAs: 'vm'
      };
    })

    .directive('abcPerson', function () {
      return {
        templateUrl: 'app/directiveExamples/abcPerson.html',
        scope: {
          dude: '=',
          editing: '='
        }
      };
    })

    .directive('abcLogo', function () {
   
      return {
        templateUrl: 'app/directiveExamples/abcLogo.html',
        transclude: true,
        scope: {
          dept: '@'
        }
      };

    })

    .controller('DirectiveExamplesController', function (people, companies) {
      var vm = this; 

      vm.departmentName = 'Engineering';

      people.getPerson(13).then(function (data) {
        vm.person = data;
      });

      companies.getCompany(1).then(function (data) {
        vm.exampleCompany = data;
      });

    })
  ;

})();
