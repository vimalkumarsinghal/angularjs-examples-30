
angular.module('sandbox', [
  'ngRoute',
  'controllerExamples',
  'watchExamples',
  'serviceExamples',
  'promiseExamples',
  'directiveExamples'
])

  .controller('MainController', function (APP_CONFIG) {
    var vm = this;
    vm.appName = APP_CONFIG.appName;
  })

  .config(function ($routeProvider, APP_CONFIG) {

    $routeProvider
      .when('/', {
        templateUrl: 'app/menu/menu.html'
      })
      .when('/controllers', {
        templateUrl: 'app/controllerExamples/controllerExamples.html'
      })
      .when('/watches', {
        templateUrl: 'app/watchExamples/watchExamples.html',
        controller: 'WatchExamplesController',
        controllerAs: 'vm'
      })
      .when('/services', {
        templateUrl: 'app/serviceExamples/serviceExamples.html'
      })
      .when('/promises', {
        templateUrl: 'app/promiseExamples/promiseExamples.html',
        controller: 'PromiseExamplesController',
        controllerAs: 'vm'
      })
      .when('/directives', {
        templateUrl: 'app/directiveExamples/directiveExamples.html',
        controller: 'DirectiveExamplesController',
        controllerAs: 'vm'
      })
      .otherwise({
        redirectTo: '/'
      })
    ;

  })
;
