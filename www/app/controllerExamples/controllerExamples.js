


angular.module('controllerExamples', [])

  .controller('FirstController', function ($scope, $log) {
    var vm = this;

    vm.firstname = 'George';

    vm.sayHello = function () {
      $log.log('Hi');
    };

    $log.log($scope);

    $log.log('Finished instantiating FirstController');
  })

  .controller('SecondController', function ($log) {

    this.firstname = 'Susan';

    $log.log('Finished instantiating SecondController');
  })

;
