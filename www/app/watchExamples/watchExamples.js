;(function () {

'use strict';

angular.module('watchExamples', [])
  .controller('WatchExamplesController', function ($scope, $log) {

    var vm = this;

    vm.doAThing = function () {
      $log.log('I am doing the thing');
    };

    vm.person = {};
/*
    $scope.$watch(
      'vm.person.firstname', 
      function () {
        vm.person.firstname = vm.person.firstname + '!';
      }
    );
*/
    $scope.$watch(
      function () { return vm.person.zip; },
      
      function (newZip) {

      if (!angular.isDefined(newZip)) {
        return;
      }
      
      if (newZip.length === 5) {

        $log.debug('Looking up ' + newZip + '...');
        
        // pretend to look up zip code in some external service...
        //
        if (newZip === '90210') {
          vm.person.city = 'Beverly Hills';
          vm.person.state = 'CA';
        }
        else {
          $log.warn('Zip ' + newZip + ' not found');
        }
      }
      else {
        vm.person.city = '';
        vm.person.state = ''
      }

    });
    
/*
    $scope.$watchGroup(
      [
        function () { return vm.person.firstname; },
        'vm.person.lastname'
      ],
      function (newValue, oldValue) {
        $log.log('Name has changed from ', oldValue, newValue);
      }
    );

    $scope.$watch(
      'vm.person',
      function (newVal, oldVal) {
        $log.log('Person object is now', vm.person);
      },
      true
    );


    $scope.$watchCollection(
      'vm.person',
      function (newVal, oldVal) {
        $log.log('Person object is now (via watch collection)', vm.person);
      }
    );

*/

  })
;

})();

