;(function () {

  'use strict';

  angular.module('serviceExamples', [])

    .constant('APP_NAME', 'Angular Examples')

    .constant('APP_CONFIG', {
      appName: 'Angular Examples',
      favoriteColor: 'mauve',
      petName: 'Rufus'
    })

    .value('employeeList', [
      { name: 'Jill' },
      { name: 'Bob' },
      { name: 'Ulysses' }
    ])

    .service('stringUtils', function () {
      var that = this;

      that.makeUppercase = function (s) {
        return s.toUpperCase();
      };

      that.makeLowercase = function (s) {
        return s.toLowerCase();
      };

    })

    .factory('stringUtilsFactory', function (reverseString) {

      return {
        makeUppercase: function (s) {
          return s.toUpperCase();
        },

        makeLowercase: function (s) {
          return s.toLowerCase();
        },

        reverseString: function (s) {
          return reverseString(s);
        }
      }
    })

    .factory('five', function () { return 5; })

    .factory('reverseString', function () {
    
      return function (s) {
        return 'pretend this is reversed: ' + s;
      }
    
    })

    .service('simpleMath', function () {
    
      function addTwoNumbers(a, b) {
        return Number(a) + Number(b);
      }

      function totalAllNumbers(numArray) {
        var total = 0;

        angular.forEach(numArray, function (num) {
          total += parseFloat(num);
        });

        return total;
      }

      return {
        add:    addTwoNumbers,
        total:  totalAllNumbers
      };
    })



    .service('simpleMathSvc', function () {

      var that = this;

      that.add = function (a, b) {
        return Number(a) + Number(b);
      };

      that.total = function (numArray) {
        var total = 0;

        angular.forEach(numArray, function (num) {
          total += parseFloat(num);
        });

        return total;
      };

    })










    .controller('AlphaController', function (
        APP_NAME,
        APP_CONFIG,
        employeeList,
        stringUtils,
        stringUtilsFactory,
        reverseString,
        five,
        $log
    ) {
      var vm = this;

      $log.log('Five is ' + five);
      
      $log.log( reverseString('this is my original string') );


      var original = 'this is a string';

      $log.log('stringUtils.makeUppercase(original) = ' +
          stringUtils.makeUppercase(original)
      );

      $log.log('stringUtilsFactory.makeUppercase(original) = ' +
          stringUtilsFactory.makeUppercase(original)
      );

      vm.appName = APP_NAME;

      vm.employeeList = employeeList;

      $log.debug(APP_CONFIG);

      // This changes the favorite color property for all places
      // where APP_CONFIG is injected.
      //
      APP_CONFIG.favoriteColor = 'greenishblue';

      // However, this *doesn't* change the value of APP_NAME everywhere that
      // it's injected, since it's a simple string.
      //
      APP_NAME = 'Fred';
    })

    .controller('BetaController', function (
        APP_NAME,
        APP_CONFIG,
        employeeList,
        $log
    ) {
      var vm = this;

      vm.showValues = function () {
        $log.debug('APP_NAME: ', APP_NAME);
        $log.debug('APP_CONFIG: ', APP_CONFIG);
      };

      vm.addNewEmployee = function () {
        employeeList.push({ name: 'Juniper' });
      };
      
    })
    
    .controller('SimpleMathExampleController', function (
      $log,
      simpleMath
    ) {
      var vm = this;

      vm.sum = 0;
      vm.numberList = [];

      vm.calculate = function () {
        vm.sum = simpleMath.add(vm.firstNumber, vm.secondNumber);
        vm.listTotal = simpleMath.total(vm.numberList);
      };

      vm.pushNumber = function (num) {
        vm.numberList.push(Number(num));
        vm.newNumber = undefined;
      };
      
    })  

    .constant('API_ROOT', 'http://localhost:3000')

    .factory('companies', function ($http, $log, API_ROOT) {
      return {
        getAllCompanies: function () {
          return $http.get(API_ROOT + '/companies/');
        },

        getCompany: function (companyId) {
          return $http.get(API_ROOT + '/companies/' + companyId)
            .then(function (response) {
              return response.data;
            })
          ;
        },

        updateCompany: function (companyId, newData) {
          return $http.put(API_ROOT + '/companies/' + companyId, newData);
        }
      };
    })

    .factory('people', function ($http, $log) {

      function getAllPeople() {
        return $http.get('http://localhost:3000/people/');
      }

      function getPerson(id) {
        return $http.get('http://localhost:3000/people/' + id)
          .then(function (response) {
            return response.data;
          })
        ;
      }

      function updatePerson(id, newData) {
        return $http.put('http://localhost:3000/people/' + id, newData);
      }

      return {
        getAllPeople: getAllPeople,
        getPerson: getPerson,
        updatePerson: updatePerson
      };
    
    })

    .controller('CompaniesController', function (companies) {
      var vm = this;

      vm.message = 'All quiet';
      
      companies.getAllCompanies().success(function (data) {
        vm.companies = data;
      });

      vm.getSingleCompany = function () {
        companies.getCompany(vm.companyId).success(function (data) {
          vm.singleCompany = data;
        });
      };

      vm.saveChanges = function () {

        vm.message = 'Saving...';

        companies.updateCompany(vm.companyId, vm.singleCompany)
          .success(function () {
            vm.message = 'All Changes Saved!';
          })
        ;
      }
    })

    .controller('PeopleController', function (people) {
      var vm = this;
      
      people.getAllPeople().success(function (data) {
        vm.people = data;
      });

      people.getPerson(127).success(function (data) {
        vm.singlePerson = data;
      });

      vm.changeSomeDetails = function () {
        vm.singlePerson.firstname = 'Senthil';

        people.updatePerson(vm.singlePerson.id, {
          firstname: vm.singlePerson.firstname
        });
        
      };

    })
  ;

})();
