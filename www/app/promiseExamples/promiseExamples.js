;(function () {

  'use strict';

  angular.module('promiseExamples', [])

    .factory('gatherPeople', function ($http, $log, $q) {
    
      return function (idList) {

        var allPeople = [];
        var peoplePromises = [];

        angular.forEach(idList, function (personId) {
          peoplePromises.push(
            $http.get('http://localhost:3000/people/' + personId)
          );
        });

        var pAll = $q.all(peoplePromises).then(function (peopleResults) {

          angular.forEach(peopleResults, function (person) {
            allPeople.push(person.data);
          });

          return allPeople;
        });

        return pAll;

      };

    })

    .controller('PeopleGathererController', function ($log, gatherPeople) {
      var vm = this;

      vm.personId = '';

      vm.peopleIdList = [];

      vm.addPersonId = function (id) {
        vm.peopleIdList.push(vm.personId);
      };

      vm.gatherPeople = function () {

        gatherPeople(vm.peopleIdList)
          .then(function (peopleData) {
            vm.allPeople = peopleData;
          })
        ;

      };
    })

    .controller('PromiseExamplesController', function (
        $http,
        $timeout,
        $interval,
        $log,
        $q
    ) {
      var vm = this;
/*
      var t = $timeout(angular.noop, 3000);

      var i = $interval(angular.noop, 500, 5);

      i.then(
        function () { $log.log('All intervals complete') },
        function () {},
        function () { $log.log('Single interval complete') }
      );


      t.then(
          function () { $log.log('Time is up!') }
        );

      var p1 = $http.get('http://localhost:3000/people/1');

      p1.then(function (response) {
        $log.log(response);
        vm.person1 = response.data;
      });

      var p2 = $http.get('http://localhost:3000/people/2');

      p2.then(
        function (response) { vm.person2 = response.data; },
        function (err) { $log.error('OH NOES', err); }
      );

      var pAll = $q.all([ p1, p2, t, i ]);

      var pAllByName = $q.all({
        firstRequest: p1,
        secondRequest: p2, 
        timeoutPromise: t,
        intervalPromise: i
      });

      pAll.then(
        function (results) {
          $log.log('All promises resolved:', results);
        }
      );

*/      
    })
  ;

})();
